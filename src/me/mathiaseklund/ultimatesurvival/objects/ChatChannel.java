package me.mathiaseklund.ultimatesurvival.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.util.Util;
import me.mathiaseklund.ultimatesurvival.util.global;

public class ChatChannel {

	/**
	 * Files
	 */
	US main;
	FileConfiguration data;

	/**
	 * ChatChannel data
	 */
	String id;
	String name;
	String format;

	int cooldown;
	int radius;

	/**
	 * HashMaps
	 */
	HashMap<String, Integer> cooldowns = new HashMap<String, Integer>();

	/**
	 * Lists
	 */
	ArrayList<String> connected = new ArrayList<String>();

	/**
	 * Called when object is created.
	 * 
	 * @param chat
	 *            id of chat channel to create
	 */
	public ChatChannel(String id) {
		main = US.getMain();
		global.chatchannels.put(id, this);
		this.data = main.getChats();
		this.id = id;
		load();
	}

	/**
	 * Connect user to chat channel
	 * 
	 * @param user
	 *            User that wants to join channel
	 */
	public void connect(User user) {
		if (!connected.contains(user.getName())) {
			connected.add(user.getName());
			user.addChatChannel(this.id);
			user.setLastChat(this.id);
			Util.message(user.getPlayer(), "&aYou've joined the " + this.name + " Chat Channel!");
		}
	}

	/**
	 * Disconnect user from chat channel
	 * 
	 * @param user
	 *            User that wants to disconnect from channel
	 */
	public void disconnect(User user) {
		if (connected.contains(user.getName())) {
			connected.remove(user.getName());
			user.removeChatChannel(this.id);
			Util.message(user.getPlayer(), "&cYou've left the " + this.name + " Chat Channel!");
		}
	}

	public boolean isConnected(User user) {
		if (connected.contains(user.getName())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Load ChatChannel data
	 */
	void load() {
		String path = "channel." + this.id;
		this.cooldown = data.getInt(path + ".cooldown");
		this.format = ChatColor.translateAlternateColorCodes('&', data.getString(path + ".format"));
		this.name = data.getString(path + ".name");
		this.radius = data.getInt(path + ".radius");
	}

	/**
	 * Try to send a message in chat channel
	 * 
	 * @param player
	 *            Player that wants to send a message
	 * @param message
	 *            Message to be sent
	 * @return true/false depending on if message is sent successfully.
	 */
	public boolean sendMessage(Player player, String message) {
		User user = global.users.get(player.getName());
		// TODO add check if user is muted in this channel
		List<String> channels = user.getChatChannels();
		if (!channels.isEmpty()) {
			if (channels.contains(this.id)) {
				if (!cooldowns.containsKey(player.getName())) {
					String format = this.format;
					if (format != null) {
						format = format.replaceAll("%channel%", this.name).replaceAll("%player%", user.getNick())
								.replaceAll("%message%", message);
						message = format;
						for (String name : connected) {
							User u = global.users.get(name);
							if (this.radius > 0) {
								double distance = u.getPlayer().getLocation().distance(user.getPlayer().getLocation());
								Util.debug("distance = " + distance);
								if (distance <= radius) {
									u.message(message);
								}
							} else {
								u.message(message);
							}
						}
						user.setLastChat(this.id);
						if (this.cooldown > 0) {
							cooldowns.put(user.getName(), this.cooldown);
							Countdown(user.getName());
						}
						return true;
					} else {
						Util.message(player, "&4ERROR:&7 Formatting error. Contact a staff member!");
						return false;
					}
				} else {
					int cooldown = cooldowns.get(player.getName());
					Util.message(player, "&4ERROR:&7 You can send a message in &7&l" + cooldown + "&7 seconds.");
					return false;
				}
			} else {
				Util.message(player, "&4ERROR:&7 You are not connected to this chat channel.");
				return false;
			}
		} else {
			Util.message(player, "&4ERROR:&7 You are not connected to any chat channels!");
			return false;
		}
	}

	/**
	 * Countdown the chat cooldown timer
	 * 
	 * @param name
	 *            Name of player who is on cooldown
	 */
	void Countdown(String name) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
			public void run() {
				int cd = cooldowns.get(name);
				if (cd <= 0) {
					cooldowns.remove(name);
				} else {
					cd--;
					cooldowns.put(name, cd);
					Countdown(name);
				}
			}
		}, 20);
	}
}
