package me.mathiaseklund.ultimatesurvival.objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.api.Items;
import me.mathiaseklund.ultimatesurvival.util.Util;
import me.mathiaseklund.ultimatesurvival.util.global;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_13_R2.NBTTagList;
import net.minecraft.server.v1_13_R2.NBTTagString;

public class CustomItem {

	US main = US.getMain();

	String id;
	String name;
	String displayname;
	ItemStack is;
	List<String> lore = new ArrayList<String>();
	String vanillaid;

	FileConfiguration items = main.getItems();

	public CustomItem(String id) {
		this.id = id;
		loadItem(id);
	}

	public CustomItem(ItemStack is) {
		this.vanillaid = is.getType().toString().toLowerCase();
		String id = is.getType().toString().toLowerCase().replaceAll("_", "");
		this.id = id;
		this.is = is;
		if (items.getConfigurationSection(id) != null) {
			loadItem(id);
		} else {
			createItem(id);
		}
	}

	/**
	 * Create a new custom item.
	 * 
	 * @param id
	 *            id of new item to create
	 */
	void createItem(String id) {
		if (items.getConfigurationSection(id) == null) {

			String name = null;
			String n = vanillaid.replaceAll("_", " ");
			for (String s : n.split(" ")) {
				if (name == null) {
					name = s.substring(0, 1).toUpperCase() + s.substring(1);
				} else {
					name = name + " " + s.substring(0, 1).toUpperCase() + s.substring(1);
				}
			}
			this.id = id;
			items.set(id + ".itemstack", is);
			items.set(id + ".name", name);
			items.set(id + ".displayname", "&e%name%");
			items.set(id + ".lore", new ArrayList<String>());
			items.set(id + ".dropchance.air", 20.0);
			items.set(id + ".dropchance.block", 20.0);
			main.saveItems();
			if (is.getType().isBlock()) {
				items.set(id + ".block.health", 5);
				items.set(id + ".block.strength", 0);
			}
			main.saveDrops();
			loadItem(id);
		} else {
			Util.debug("Tried to create a new item with an existing ID.");
		}

	}

	public int getHealth() {
		return items.getInt(id + ".block.health");
	}

	public

	/**
	 * Load item data from items file and apply to itemstack
	 * 
	 * @param id
	 */
	void loadItem(String id) {

		this.name = items.getString(id + ".name");
		this.displayname = items.getString(id + ".displayname");

		List<String> lore = items.getStringList(id + ".lore");
		for (String s : lore) {
			this.lore.add(ChatColor.translateAlternateColorCodes('&', s));
		}

		ItemStack is = items.getItemStack(id + ".itemstack");
		if (is == null) {
			String type = items.getString(id + ".type");
			is = new ItemStack(Material.getMaterial(type));
		}
		is = new ItemStack(is);
		ItemMeta im = is.getItemMeta();
		im.addItemFlags(ItemFlag.HIDE_PLACED_ON);
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		im.setLocalizedName(Util.HideString("[ultimatesurvival.item] " + id));
		im.setDisplayName(
				ChatColor.translateAlternateColorCodes('&', this.displayname.replaceAll("%name%", this.name)));
		im.setLore(this.lore);
		is.setItemMeta(im);
		this.is = is;

		global.customitems.put(this.id, this);
	}

	/**
	 * Get the itemstack associated with this custom item
	 * 
	 * @return ItemStack is
	 */
	public ItemStack getItem() {
		ItemStack is = new ItemStack(this.is);
		if (is.getType().isBlock()) {
			net.minecraft.server.v1_13_R2.ItemStack stack = CraftItemStack.asNMSCopy(is);
			if (stack.getTag() != null) {
				NBTTagList tags = (NBTTagList) stack.getTag().get("CanPlaceOn");

				if (tags == null) {
					tags = new NBTTagList();
				}

				for (Material mat : Material.values()) {
					if (mat.isBlock()) {
						tags.add(new NBTTagString("minecraft:" + mat.toString().toLowerCase()));
					}
				}

				stack.getTag().set("CanPlaceOn", tags);

				is = CraftItemStack.asCraftMirror(stack);
			}
		}
		return is;
	}

	public double getDropChance(String tool) {
		if (tool == null) {
			tool = "air";
		} else if (Material.getMaterial(Items.getItem(tool).getType().toString()).isBlock()) {
			tool = "block";
		}
		return items.getDouble(this.id + ".dropchance." + tool);
	}
}
