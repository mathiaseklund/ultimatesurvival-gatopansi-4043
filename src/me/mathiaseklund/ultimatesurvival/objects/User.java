package me.mathiaseklund.ultimatesurvival.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.enums.Role;
import me.mathiaseklund.ultimatesurvival.util.Util;
import me.mathiaseklund.ultimatesurvival.util.global;

public class User {

	/**
	 * Files
	 */
	FileConfiguration data;
	US main;

	/**
	 * User Data
	 */
	Role role;

	Player player;

	String uuid;
	String name;
	String nickname;

	boolean godmode;

	// Chat data
	String lastchat;
	List<String> chatchannels = new ArrayList<String>();

	/**
	 * Called when object is created.
	 * 
	 * @param player
	 *            Player object is created for.
	 */
	public User(Player player) {
		this.main = US.getMain();
		this.data = main.getUsers();
		this.player = player;
		this.uuid = player.getUniqueId().toString();
		this.name = player.getName();

		global.users.put(player.getName(), this);

		load();
	}

	public boolean isStaff() {
		if (this.role == Role.STAFF) {
			return true;
		} else {
			return false;
		}
	}

	public void openInteractMenu(Player player) {
		Inventory inv = Bukkit.createInventory(null, 54, this.nickname);

		ItemStack trade = new ItemStack(Material.EMERALD);
		ItemMeta tim = trade.getItemMeta();
		tim.setDisplayName(Util.HideString("[interact.trade]-" + this.name + " &aTrade"));
		trade.setItemMeta(tim);
		inv.addItem(trade);

		player.openInventory(inv);
	}

	/**
	 * Load stored Player Data
	 */
	void load() {
		if (data.get(this.uuid) != null) {
			String path = this.uuid;
			// Player has played before.
			this.role = Role.valueOf(data.getString(path + ".role"));
			this.lastchat = data.getString(path + ".lastchat");
			this.nickname = data.getString(path + ".nickname");
			if (nickname == null) {
				nickname = this.name;
			}

			for (String chat : data.getStringList(path + ".chatchannels")) {
				Util.debug("Found ChatChannel: " + chat);
				ChatChannel channel = global.chatchannels.get(chat);
				channel.connect(this);
			}

			this.godmode = data.getBoolean(path + ".godmode");
			String locString = data.getString(path + ".location");
			if (locString == null) {
				locString = main.getConfig().getString("spawn.startingvillage.location");
				Util.debug("locString = " + locString);
				player.teleport(Util.StringToLocation(locString));
			}

		} else {
			// Player is a new player.
			createUser();
		}
	}

	/**
	 * Get player object linked to user
	 * 
	 * @return Player player
	 */
	public Player getPlayer() {
		return this.player;
	}

	/**
	 * Connect to a chat channel
	 * 
	 * @param id
	 *            id of chat channels
	 */
	public void addChatChannel(String id) {
		if (!chatchannels.contains(id)) {
			Util.debug("Adding Chat Channel to user.");
			chatchannels.add(id);
		}
	}

	/**
	 * Disconnect from a chat channel
	 * 
	 * @param id
	 *            id of chat channel
	 */
	public void removeChatChannel(String id) {
		if (chatchannels.contains(id)) {
			Util.debug("Removing Chat Channel from user.");
			chatchannels.remove(id);
		}
	}

	/**
	 * Create data for a new user
	 */
	void createUser() {

		String path = this.uuid;
		/**
		 * Set the default user data
		 */
		data.set(path + ".role", Role.USER.toString());

		/**
		 * Set the defaultly joined chat channels
		 */
		ArrayList<String> chats = new ArrayList<String>();
		chats.add("global");
		data.set(path + ".chatchannels", chats);

		main.saveUsers();
		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
			public void run() {
				load();
			}
		}, 1);
	}

	/**
	 * Get player default name
	 * 
	 * @return String name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Get users UniqueID String
	 * 
	 * @return String uuid
	 */
	public String getUUID() {
		return this.uuid;
	}

	/**
	 * Get player nickname
	 * 
	 * @return String nickname
	 */
	public String getNick() {
		return this.nickname;
	}

	/**
	 * Get user role
	 * 
	 * @return Role role
	 */
	public Role getRole() {
		return this.role;
	}

	/**
	 * Player logged out of server.
	 */
	public void logout() {
		global.users.remove(this.name);
		for (String chat : getChatChannels()) {
			ChatChannel channel = global.chatchannels.get(chat);
			channel.connected.remove(this.getName());
		}
		store();
	}

	/**
	 * Store required user data
	 */
	void store() {
		String path = this.uuid;
		data.set(path + ".role", this.role.toString());
		data.set(path + ".nickname", this.nickname);
		data.set(path + ".lastonline", new Date().toString());
		data.set(path + ".lastchat", this.lastchat);
		data.set(path + ".chatchannels", this.chatchannels);
		data.set(path + ".godmode", this.godmode);
		data.set(path + ".location", Util.LocationToString(this.player.getLocation()));
		main.saveUsers();
	}

	/**
	 * Get current joined chat channels
	 * 
	 * @return List of joined chat channels
	 */
	public List<String> getChatChannels() {
		return this.chatchannels;
	}

	/**
	 * Send message in the chat window to player.
	 * 
	 * @param message
	 *            Message to be sent
	 */
	public void message(String message) {
		this.player.sendMessage(message);
	}

	/**
	 * Get the last chat channel player sent a message in.
	 */
	public String getLastChatChannel() {
		return this.lastchat;
	}

	/**
	 * Set the last chat channel user sent a message in
	 * 
	 * @param id
	 *            id of chat channel
	 */
	public void setLastChat(String id) {
		this.lastchat = id;
	}

	/**
	 * Toggle GodMode for User
	 */
	public void toggleGodmode() {
		if (this.godmode) {
			this.godmode = false;
			Util.message(this.player, "&6God mode &cdisabled&6!");
		} else {
			this.godmode = true;
			heal();
			feed();
			Util.message(this.player, "&6God mode &cenabled&6!");
		}
	}

	/**
	 * Heal user to max health
	 */
	@SuppressWarnings("deprecation")
	public void heal() {
		this.player.setHealth(this.player.getMaxHealth());
		Util.message(this.player, "&6You've been &chealed&6!");
	}

	/**
	 * Set food level to max
	 */
	public void feed() {
		this.player.setFoodLevel(20);
		Util.message(this.player, "&6You've been &cfed&6!");
	}

	/**
	 * Check if user has godmode enabled
	 * 
	 * @return true/false
	 */
	public boolean hasGodmode() {
		return this.godmode;
	}

	//
}
