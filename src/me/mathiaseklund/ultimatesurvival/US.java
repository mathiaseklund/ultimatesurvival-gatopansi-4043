package me.mathiaseklund.ultimatesurvival;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;

import me.mathiaseklund.ultimatesurvival.api.Crafting;
import me.mathiaseklund.ultimatesurvival.api.Items;
import me.mathiaseklund.ultimatesurvival.commands.setup.CommandDrops;
import me.mathiaseklund.ultimatesurvival.commands.setup.CommandRecipe;
import me.mathiaseklund.ultimatesurvival.commands.staff.CommandFeed;
import me.mathiaseklund.ultimatesurvival.commands.staff.CommandGod;
import me.mathiaseklund.ultimatesurvival.commands.staff.CommandHeal;
import me.mathiaseklund.ultimatesurvival.commands.staff.CommandItem;
import me.mathiaseklund.ultimatesurvival.commands.user.CommandGlobal;
import me.mathiaseklund.ultimatesurvival.commands.user.CommandLocal;
import me.mathiaseklund.ultimatesurvival.listeners.BlockListener;
import me.mathiaseklund.ultimatesurvival.listeners.ChatListener;
import me.mathiaseklund.ultimatesurvival.listeners.CraftingListener;
import me.mathiaseklund.ultimatesurvival.listeners.EntityListener;
import me.mathiaseklund.ultimatesurvival.listeners.GodmodeListener;
import me.mathiaseklund.ultimatesurvival.listeners.ItemListener;
import me.mathiaseklund.ultimatesurvival.listeners.LoginListener;
import me.mathiaseklund.ultimatesurvival.objects.ChatChannel;
import me.mathiaseklund.ultimatesurvival.objects.User;
import me.mathiaseklund.ultimatesurvival.util.Util;
import me.mathiaseklund.ultimatesurvival.util.global;

public class US extends JavaPlugin {

	static US main;
	public ProtocolManager protocolManager;

	/**
	 * YML Files
	 */
	File usersFile;
	FileConfiguration users;

	File chatsFile;
	FileConfiguration chats;

	File recipesFile;
	FileConfiguration recipes;

	File itemsFile;
	FileConfiguration items;

	File dropsFile;
	FileConfiguration drops;

	/**
	 * Get instance of Main class
	 * 
	 * 
	 * @return Main class
	 */
	public static US getMain() {
		return main;
	}

	/**
	 * Plugin is enabled
	 */
	public void onEnable() {
		main = this;

		protocolManager = ProtocolLibrary.getProtocolManager();

		LoadYMLFiles();
		RegisterListeners();
		RegisterCommands();

		Crafting.loadRecipes();
		Crafting.setupWorkbench();
		LoginPlayers();

		for (Material mat : Material.values()) {
			if (mat != Material.AIR) {
				ItemStack is = new ItemStack(mat);
				Items.convertToCustomItem(is);
			}
		}
	}

	/**
	 * Load all chat channels
	 */
	void LoadChatChannels() {
		if (chats != null) {
			for (String chat : chats.getConfigurationSection("channel").getKeys(false)) {
				// TODO Create chat channels
				Util.debug("Creating chat channel - " + chat);
				new ChatChannel(chat);
			}
		}
	}

	/**
	 * Plugin is disabled
	 */
	public void onDisable() {
		LogoutPlayers();
	}

	/**
	 * Log out all online players.
	 */
	void LogoutPlayers() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			Util.debug(p.getName() + " is being logged out.");
			global.users.get(p.getName()).logout();
		}
	}

	/**
	 * Load default .yml files
	 */
	void LoadYMLFiles() {
		this.saveDefaultConfig();

		/**
		 * Users file
		 */
		usersFile = new File(getDataFolder(), "users.yml");
		if (!usersFile.exists()) {
			usersFile.getParentFile().mkdirs();
			saveResource("users.yml", false);
		}

		users = new YamlConfiguration();
		try {
			users.load(usersFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		/**
		 * Drops file
		 */
		dropsFile = new File(getDataFolder(), "drops.yml");
		if (!dropsFile.exists()) {
			dropsFile.getParentFile().mkdirs();
			saveResource("drops.yml", false);
		}

		drops = new YamlConfiguration();
		try {
			drops.load(dropsFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}

		/**
		 * ChatChannels file
		 */
		chatsFile = new File(getDataFolder(), "chats.yml");
		if (!chatsFile.exists()) {
			chatsFile.getParentFile().mkdirs();
			saveResource("chats.yml", false);
		}

		chats = new YamlConfiguration();
		try {
			chats.load(chatsFile);

		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		} finally {
			LoadChatChannels();
		}

		/**
		 * Recipes file
		 */
		recipesFile = new File(getDataFolder(), "recipes.yml");
		if (!recipesFile.exists()) {
			recipesFile.getParentFile().mkdirs();
			saveResource("recipes.yml", false);
		}

		recipes = new YamlConfiguration();
		try {
			recipes.load(recipesFile);

		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		} finally {
			Crafting.loadRecipes();
		}

		/**
		 * Items file
		 */
		itemsFile = new File(getDataFolder(), "items.yml");
		if (!itemsFile.exists()) {
			itemsFile.getParentFile().mkdirs();
			saveResource("items.yml", false);
		}

		items = new YamlConfiguration();
		try {
			items.load(itemsFile);

		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		} finally {
			Items.loadItems();
		}
	}

	/**
	 * Get users data file
	 * 
	 * @return users data file
	 */
	public FileConfiguration getUsers() {
		return this.users;
	}

	/**
	 * Get items data file
	 * 
	 * @return items data file
	 */
	public FileConfiguration getItems() {
		return this.items;
	}

	/**
	 * Save users yml file
	 */
	public void saveItems() {
		try {
			items.save(itemsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveDrops() {
		try {
			drops.save(dropsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save items yml file
	 */
	public void saveUsers() {
		try {
			users.save(usersFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save chats yml file
	 */
	public void saveChats() {
		try {
			chats.save(chatsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save recipes yml file
	 */
	public void saveRecipes() {
		try {
			recipes.save(recipesFile);
			Crafting.loadRecipes();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get recipes data file
	 * 
	 * @return
	 */
	public FileConfiguration getRecipes() {
		return this.recipes;
	}

	public FileConfiguration getDrops() {
		return this.drops;
	}

	/**
	 * Get chats data file
	 * 
	 * @return chats data file
	 */
	public FileConfiguration getChats() {
		return this.chats;
	}

	/**
	 * Register Event Listener classes
	 */
	void RegisterListeners() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new LoginListener(), this);
		pm.registerEvents(new ChatListener(), this);
		pm.registerEvents(new GodmodeListener(), this);
		pm.registerEvents(new EntityListener(), this);
		pm.registerEvents(new CraftingListener(), this);
		pm.registerEvents(new ItemListener(), this);
		pm.registerEvents(new BlockListener(), this);
	}

	/**
	 * Register command executors
	 */
	void RegisterCommands() {

		/**
		 * Configuration Commands
		 */
		getCommand("recipe").setExecutor(new CommandRecipe());
		getCommand("drops").setExecutor(new CommandDrops());

		/**
		 * Staff Commands
		 */
		getCommand("heal").setExecutor(new CommandHeal());
		getCommand("god").setExecutor(new CommandGod());
		getCommand("feed").setExecutor(new CommandFeed());
		getCommand("item").setExecutor(new CommandItem());

		/**
		 * User Commands
		 */
		getCommand("global").setExecutor(new CommandGlobal());
		getCommand("local").setExecutor(new CommandLocal());
	}

	/**
	 * Log in all online players.
	 */
	void LoginPlayers() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			Util.debug(p.getName() + " is being logged in.");
			new User(p);
		}
	}
}
