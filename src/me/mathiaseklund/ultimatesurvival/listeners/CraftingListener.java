package me.mathiaseklund.ultimatesurvival.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.api.Crafting;
import me.mathiaseklund.ultimatesurvival.util.Util;

public class CraftingListener implements Listener {

	US main = US.getMain();

	@EventHandler
	public void onPrepareCraft(PrepareItemCraftEvent event) {
		// Cancel all vanilla crafting.
		event.getInventory().setResult(null);
	}

	@EventHandler
	public void onWorkbenchClick(InventoryClickEvent event) {

		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			if (event.getClickedInventory() != null) {
				String title = event.getInventory().getTitle();
				title = title.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
				if (title.contains("[wb]")) {
					ItemStack is = event.getCurrentItem();
					if (is != null) {
						if (is.hasItemMeta()) {
							ItemMeta im = is.getItemMeta();
							if (im.hasLocalizedName()) {
								String dname = im.getLocalizedName();
								dname = dname.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
								if (dname.contains("[wb.outline]") || dname.contains("[wb.craftoption]")) {
									event.setCancelled(true);
								} else if (dname.contains("[wb.craftresult] ")) {
									event.setCancelled(true);
									Util.debug("Prepare to craft item");
									String recipe = dname.split(" ")[1];
									Crafting.craftItem(player, event.getInventory(), recipe);
								}
							}
						}
					}
					Bukkit.getScheduler().scheduleSyncDelayedTask(US.getMain(), new Runnable() {
						public void run() {
							Crafting.checkWBCraftOptions(player, event.getInventory());
						}
					}, 1);
				}
			}
		}
	}

	@EventHandler
	public void onWorkbenchDrag(InventoryDragEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			if (event.getInventory() != null) {
				String title = event.getInventory().getTitle();
				title = title.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
				if (title.contains("[wb]")) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(US.getMain(), new Runnable() {
						public void run() {
							Crafting.checkWBCraftOptions(player, event.getInventory());
						}
					}, 1);
				}
			}
		}
	}

	@EventHandler
	public void onCloseWorkbench(InventoryCloseEvent event) {
		if (event.getPlayer() instanceof Player) {
			Player player = (Player) event.getPlayer();
			String title = event.getInventory().getTitle();
			title = title.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
			if (title.contains("[wb]")) {
				for (ItemStack is : event.getInventory().getContents()) {
					if (is != null) {
						if (is.hasItemMeta()) {
							ItemMeta im = is.getItemMeta();
							if (im.hasLocalizedName()) {
								String dname = im.getLocalizedName();
								dname = dname.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
								if (dname.contains("[wb")) {
									continue;
								}
							}
						}
						player.getInventory().addItem(is);
					}
				}
			}
		}
	}
}
