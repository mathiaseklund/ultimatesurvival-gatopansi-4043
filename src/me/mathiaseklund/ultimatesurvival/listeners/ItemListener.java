package me.mathiaseklund.ultimatesurvival.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemListener implements Listener {

	@EventHandler
	public void onItemSpawn(ItemSpawnEvent event) {
		ItemStack is = event.getEntity().getItemStack();
		if (is != null) {
			if (is.hasItemMeta()) {
				ItemMeta im = is.getItemMeta();
				if (im.hasLocalizedName()) {
					String lname = im.getLocalizedName();
					lname = lname.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
					if (!lname.contains("[ultimatesurvival.item]")) {
						event.setCancelled(true);
					}
				} else {
					event.setCancelled(true);
				}
			} else {
				event.setCancelled(true);
			}
		}
	}
}
