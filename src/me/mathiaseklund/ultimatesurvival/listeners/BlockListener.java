package me.mathiaseklund.ultimatesurvival.listeners;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerAnimationType;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.BlockPosition;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.api.Drops;
import me.mathiaseklund.ultimatesurvival.api.Items;
import me.mathiaseklund.ultimatesurvival.util.Util;
import me.mathiaseklund.ultimatesurvival.util.global;

public class BlockListener implements Listener {

	US main = US.getMain();

	HashMap<String, Integer> blockdamage = new HashMap<String, Integer>(); // location string, damage
	ArrayList<String> placecooldown = new ArrayList<String>(); // player
	ArrayList<String> placingblock = new ArrayList<String>();

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (!placingblock.contains(player.getName())) {
				placingblock.add(player.getName());
				Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
					public void run() {
						placingblock.remove(player.getName());
					}
				}, 1);
			}
		}
	}

	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		Player player = event.getPlayer();
		if (!placingblock.contains(player.getName())) {
			placingblock.add(player.getName());
			Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
				public void run() {
					placingblock.remove(player.getName());
				}
			}, 1);
		}
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player) {
			Player player = (Player) event.getDamager();
			if (!placingblock.contains(player.getName())) {
				placingblock.add(player.getName());
				Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
					public void run() {
						placingblock.remove(player.getName());
					}
				}, 1);
			}
		}
	}

	@EventHandler
	public void onPlayerAnimation(PlayerAnimationEvent event) {
		Player player = event.getPlayer();
		if (!placingblock.contains(player.getName())) {
			if (player.getGameMode() == GameMode.ADVENTURE) {
				if (event.getAnimationType() == PlayerAnimationType.ARM_SWING) {
					Block block = player.getTargetBlock(null, 4);
					if (block != null) {
						Material type = block.getType();
						if (type != Material.AIR) {
							ClickBlock(player, block);
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		if (!placingblock.contains(player.getName())) {
			placingblock.add(player.getName());
			Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
				public void run() {
					placingblock.remove(event.getPlayer().getName());
				}
			}, 1);
		}
		event.getBlock().setMetadata("placed", new FixedMetadataValue(main, player.getUniqueId().toString()));
		event.getBlock().setMetadata("itemId", new FixedMetadataValue(main, Items.getItemId(event.getItemInHand())));

	}

	/**
	 * Get blockface of target block
	 * 
	 * @param player
	 * @return
	 */
	public BlockFace getBlockFace(Player player) {
		List<Block> lastTwoTargetBlocks = player.getLastTwoTargetBlocks(null, 100);
		if (lastTwoTargetBlocks.size() != 2 || !lastTwoTargetBlocks.get(1).getType().isOccluding())
			return null;
		Block targetBlock = lastTwoTargetBlocks.get(1);
		Block adjacentBlock = lastTwoTargetBlocks.get(0);
		return targetBlock.getFace(adjacentBlock);
	}

	/**
	 * Player clicks on a block
	 * 
	 * @param player
	 * @param block
	 */
	public void ClickBlock(Player player, Block block) {
		String location = Util.LocationToString(block.getLocation());

		int damage = 1;
		if (blockdamage.containsKey(location)) {
			damage = blockdamage.get(location);
			damage++;
			blockdamage.put(location, damage);
		} else {
			blockdamage.put(location, damage);
		}
		String itemId = null;
		if (block.hasMetadata("itemId")) {
			itemId = block.getMetadata("itemId").get(0).asString();
		}
		if (itemId == null) {
			ItemStack is = new ItemStack(block.getType());
			itemId = Items.getItemId(is);
			if (itemId == null) {
				is = Items.convertToCustomItem(is);
				itemId = Items.getItemId(is);
			}
		}
		int health = global.customitems.get(itemId).getHealth();
		float percent = ((health - damage) * 100.0f) / health;
		int bh = 0;
		if (percent < 100) {
			if (percent < 90) {
				if (percent < 80) {
					if (percent < 70) {
						if (percent < 60) {
							if (percent < 50) {
								if (percent < 40) {
									if (percent < 30) {
										if (percent < 20) {
											if (percent < 10) {
												bh = 10;
											}
										} else {
											bh = 9;
										}
									} else {
										bh = 8;
									}
								} else {
									bh = 7;
								}
							} else {
								bh = 6;
							}
						} else {
							bh = 5;
						}
					} else {
						bh = 4;
					}
				} else {
					bh = 3;
				}
			} else {
				bh = 2;
			}
		} else {
			bh = 1;
		}
		if (damage < health) {
			player.playSound(player.getLocation(), Sound.BLOCK_GRASS_BREAK, 1, 1);
			int id = blockdamage.size();
			PacketContainer blockBreakAnim = new PacketContainer(PacketType.Play.Server.BLOCK_BREAK_ANIMATION);
			blockBreakAnim.getBlockPositionModifier().write(0,
					new BlockPosition(block.getX(), block.getY(), block.getZ())); // position
			blockBreakAnim.getIntegers().write(0, id); // entityid
			blockBreakAnim.getIntegers().write(1, bh);
			try {
				main.protocolManager.sendServerPacket(player, blockBreakAnim);
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			blockdamage.remove(location);

			BreakBlock(player, block);
		}
	}

	/**
	 * Break block
	 * 
	 * @param player
	 * @param block
	 */
	public void BreakBlock(Player player, Block block) {
		boolean breakBlock = true;
		if (breakBlock) {
			block.getWorld().spawnParticle(Particle.BLOCK_CRACK, block.getLocation().getX(), block.getLocation().getY(),
					block.getLocation().getZ(), 25, 0.5, 0.5, 0.5, block.getBlockData());
			if (block.hasMetadata("itemId") && block.hasMetadata("placed")) {
				String itemId = block.getMetadata("itemId").get(0).asString();
				String placed = block.getMetadata("placed").get(0).asString();
				if (placed.equalsIgnoreCase(player.getUniqueId().toString())) {
					Drops.drop(block.getLocation(), itemId);
				} else {
					Drops.tryDrop(block.getLocation(), player, itemId);
				}
			} else {
				ItemStack is = Items.convertToCustomItem(new ItemStack(block.getType()));
				String itemId = Items.getItemId(is);
				Drops.tryDrop(block.getLocation(), player, itemId);
			}
			player.playSound(player.getLocation(), Sound.BLOCK_GRASS_PLACE, 1, 1);
			block.breakNaturally();

		}
	}

}
