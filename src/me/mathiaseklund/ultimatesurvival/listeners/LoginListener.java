package me.mathiaseklund.ultimatesurvival.listeners;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.objects.User;
import me.mathiaseklund.ultimatesurvival.util.global;

public class LoginListener implements Listener {

	US main = US.getMain();

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.setJoinMessage(null);
		Player player = event.getPlayer();
		player.setGameMode(GameMode.ADVENTURE);

		// Log the player in
		new User(player);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		event.setQuitMessage(null);
		Player player = event.getPlayer();

		// Log the player out
		global.users.get(player.getName()).logout();
	}
}
