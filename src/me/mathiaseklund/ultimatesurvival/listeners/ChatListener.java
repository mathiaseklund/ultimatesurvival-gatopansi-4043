package me.mathiaseklund.ultimatesurvival.listeners;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.objects.ChatChannel;
import me.mathiaseklund.ultimatesurvival.objects.User;
import me.mathiaseklund.ultimatesurvival.util.Util;
import me.mathiaseklund.ultimatesurvival.util.global;

public class ChatListener implements Listener {

	US main = US.getMain();

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		if (!event.isCancelled()) {
			event.setCancelled(true);
			Player player = event.getPlayer();
			String message = event.getMessage();
			User user = global.users.get(player.getName());
			String channel = user.getLastChatChannel();
			if (channel != null) {
				ChatChannel chat = global.chatchannels.get(channel);
				chat.sendMessage(player, message);
			} else {
				List<String> channels = user.getChatChannels();
				if (channels.isEmpty()) {
					Util.message(player, "&4ERROR:&7 You are not connected to any chat channels!");
				} else {
					ChatChannel chat = global.chatchannels.get(channels.get(0));
					chat.sendMessage(player, message);
				}
			}
		}
	}
}
