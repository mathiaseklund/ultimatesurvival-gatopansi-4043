package me.mathiaseklund.ultimatesurvival.commands.staff;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.ultimatesurvival.api.Items;
import me.mathiaseklund.ultimatesurvival.objects.CustomItem;
import me.mathiaseklund.ultimatesurvival.objects.User;
import me.mathiaseklund.ultimatesurvival.util.Util;
import me.mathiaseklund.ultimatesurvival.util.global;

public class CommandItem implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = global.users.get(player.getName());
			if (user.isStaff()) {
				if (args.length <= 0) {
					// Convert item in hand to custom one.
					ItemStack is = player.getInventory().getItemInMainHand();
					if (is != null) {
						is = Items.convertToCustomItem(is);
						player.getInventory().setItemInMainHand(is);
						Util.message(sender, "&aSuccessfully converted item!");
					} else {
						Util.message(player, "&4ERROR:&7 Must hold an item in hand to use this command.");
					}
				} else {
					String id = args[0];
					ItemStack is = null;
					if (global.customitems.containsKey(id)) {
						CustomItem ci = global.customitems.get(id);
						is = ci.getItem();
					} else {
						boolean found = false;
						for (String s : global.customitems.keySet()) {
							if (s.contains(id)) {
								found = true;
								CustomItem ci = global.customitems.get(s);
								is = ci.getItem();
								break;
							}
						}
						if (!found) {
							Util.message(sender, "&4ERROR:&7 No item found with the id " + id + ".");
						}
					}
					if (args.length == 2) {
						if (is != null) {
							int amount = Integer.parseInt(args[1]);
							is.setAmount(amount);
						}
					}
					if (is != null) {
						player.getInventory().addItem(is);
						Util.message(player, "&cYou've been given &6x" + is.getAmount() + " "
								+ is.getItemMeta().getDisplayName() + "&6.");
					}
				}

			}
		}
		return false;
	}

}
