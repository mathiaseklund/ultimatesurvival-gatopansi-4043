package me.mathiaseklund.ultimatesurvival.commands.staff;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.objects.User;
import me.mathiaseklund.ultimatesurvival.util.global;

public class CommandFeed implements CommandExecutor {

	US main = US.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = global.users.get(player.getName());
			if (args.length == 0) {
				if (user.isStaff()) {
					user.feed();
				}
			}
		}
		return false;
	}

}
