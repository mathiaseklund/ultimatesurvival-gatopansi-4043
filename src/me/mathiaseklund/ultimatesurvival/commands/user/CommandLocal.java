package me.mathiaseklund.ultimatesurvival.commands.user;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.objects.ChatChannel;
import me.mathiaseklund.ultimatesurvival.objects.User;
import me.mathiaseklund.ultimatesurvival.util.global;

public class CommandLocal implements CommandExecutor {

	US main = US.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			User user = global.users.get(player.getName());
			ChatChannel channel = global.chatchannels.get("local");
			if (args.length == 0) {
				if (channel.isConnected(user)) {
					channel.disconnect(user);
				} else {
					channel.connect(user);
				}
			} else {
				String message = null;
				for (String s : args) {
					if (message == null) {
						message = s;
					} else {
						message += " " + s;
					}
				}
				if (!channel.isConnected(user)) {
					channel.connect(user);
				}
				channel.sendMessage(player, message);
			}
		}
		return false;
	}

}
