package me.mathiaseklund.ultimatesurvival.commands.setup;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.api.Crafting;
import me.mathiaseklund.ultimatesurvival.api.Items;
import me.mathiaseklund.ultimatesurvival.util.Util;

public class CommandRecipe implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.isOp()) {
				if (args.length > 0) {
					// Set result of recipe
					if (args[0].equalsIgnoreCase("result")) {
						if (args.length == 2) {

							String recipe = args[1];
							ItemStack is = player.getInventory().getItemInMainHand();
							if (is != null) {
								US.getMain().getRecipes().set(recipe + ".result", is);
								US.getMain().saveRecipes();
								Util.message(player,
										"&aSuccessfully set the crafting result for recipe &7" + recipe + "&a.");
							} else {
								Util.message(player, "&4ERROR: You need to have an item in hand.");
							}
						} else {
							Util.message(sender, "&eUsage:&7 /recipe result <recipe>");
						}

						// Create new recipe
					} else if (args[0].equalsIgnoreCase("create")) {
						if (args.length == 2) {
							String recipe = args[1];
							if (!Crafting.recipeExists(recipe)) {
								Crafting.createRecipe(recipe);
								Util.message(player, "&eCreated new recipe: " + recipe);
							} else {
								Util.message(player, "&4ERROR:&7 A recipe already exists with the Id: " + recipe);
							}
						} else {
							Util.message(player, "&7Usage:&e /recipe create <recipe>");
						}
					} else if (args[0].equalsIgnoreCase("delete")) {
						if (args.length == 2) {
							String recipe = args[1];
							if (Crafting.recipeExists(recipe)) {
								Crafting.deleteRecipe(recipe);
								Util.message(player, "&eDeleted Recipe: " + recipe);
							} else {
								Util.message(player, "&4ERROR:&7 A recipe doesn't exist with that Id: " + recipe);
							}
						} else {
							Util.message(player, "&7Usage:&e /recipe delete <recipe>");
						}

						// Activate recipe
					} else if (args[0].equalsIgnoreCase("activate")) {
						if (args.length == 2) {
							String recipe = args[1];
							if (Crafting.recipeExists(recipe)) {
								Crafting.activateRecipe(recipe);
								Util.message(player, "&eActivated Recipe: " + recipe);
							} else {
								Util.message(player, "&4ERROR:&7 A recipe doesn't exist with that Id: " + recipe);
							}
						} else {
							Util.message(player, "&7Usage:&e /recipe activate <recipe>");
						}
					} else if (args[0].equalsIgnoreCase("material")) { // material <recipe> add/remove <itemId> <amount>
						if (args.length == 4) {
							if (args[2].equalsIgnoreCase("add")) {
								String recipe = args[1];
								if (Crafting.recipeExists(recipe)) {
									String itemId = args[3];
									if (Items.itemExists(itemId)) {
										int amount = 1;
										Crafting.addMaterial(recipe, itemId, amount);
										Util.message(player, "&aAdded material - " + amount + "x " + itemId
												+ " - to recipe: " + recipe);
									} else {
										Util.message(player, "&4ERROR:&7 Provided itemId does not exist.");
									}
								} else {
									Util.message(player, "&4ERROR:&7 A recipe doesn't exist with that Id: " + recipe);
								}
							} else if (args[2].equalsIgnoreCase("remove")) {
								String recipe = args[1];
								if (Crafting.recipeExists(recipe)) {
									String itemId = args[3];
									if (Items.itemExists(itemId)) {
										Crafting.removeMaterial(recipe, itemId);
										Util.message(player,
												"&aRemoved material - " + itemId + " - from recipe: " + recipe);
									} else {
										Util.message(player, "&4ERROR:&7 Provided itemId does not exist.");
									}
								} else {
									Util.message(player, "&4ERROR:&7 A recipe doesn't exist with that Id: " + recipe);
								}
							} else {
								Util.message(player, "&7Usage:&e /recipe material <recipe> add/remove <itemId> <amount>");
							}
						} else if (args.length == 5) {

						} else {
							Util.message(player, "&7Usage:&e /recipe material <recipe> add <itemId> <amount>");
						}
					}
				}
			}
		}
		return false;
	}

}
