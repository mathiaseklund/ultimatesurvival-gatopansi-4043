package me.mathiaseklund.ultimatesurvival.commands.setup;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.api.Items;
import me.mathiaseklund.ultimatesurvival.util.Util;

public class CommandDrops implements CommandExecutor {

	US main = US.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.isOp()) {
				if (args.length > 0) {
					if (args[0].equalsIgnoreCase("add")) {
						if (args.length == 3) {
							String itemId = args[1];
							String dropId = args[2];
							if (Items.itemExists(itemId)) {
								if (Items.itemExists(dropId)) {
									Util.debug("itemId - " + itemId);
									Util.debug("dropId - " + dropId);
									Items.addDrop(itemId, dropId);
									Util.message(player, "&aAdded " + dropId + " to " + itemId + " drop list.");
								} else {
									Util.message(player, "&4ERROR: &7" + dropId + " doesn't exist. Try again.");
								}
							} else {
								Util.message(player, "&4ERROR: &7" + itemId + " doesn't exist. Try again.");
							}
						} else {
							Util.message(player, "&7Usage:&e /drops add <itemId> <dropid>");
						}
					} else if (args[0].equalsIgnoreCase("remove")) {
						if (args.length == 3) {
							String itemId = args[1];
							String dropId = args[2];
							if (Items.itemExists(itemId)) {
								if (Items.itemExists(dropId)) {
									Items.removeDrop(itemId, dropId);
									Util.message(player, "&aRemoved " + dropId + " to " + itemId + " drop list.");
								} else {
									Util.message(player, "&4ERROR: &7" + dropId + " doesn't exist. Try again.");
								}
							} else {
								Util.message(player, "&4ERROR: &7" + itemId + " doesn't exist. Try again.");
							}
						} else {
							Util.message(player, "&7Usage:&e /drops remove <itemId> <dropid>");
						}
					}
				}
			}
		} else {
			Util.message(sender, "&4ERROR:&7 Command can only be run by a player.");
		}
		return false;
	}

}
