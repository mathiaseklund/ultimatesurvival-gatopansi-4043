package me.mathiaseklund.ultimatesurvival.util;

import java.util.HashMap;

import me.mathiaseklund.ultimatesurvival.objects.ChatChannel;
import me.mathiaseklund.ultimatesurvival.objects.CustomItem;
import me.mathiaseklund.ultimatesurvival.objects.User;

public class global {

	public static HashMap<String, User> users = new HashMap<String, User>();
	public static HashMap<String, ChatChannel> chatchannels = new HashMap<String, ChatChannel>();
	public static HashMap<String, CustomItem> customitems = new HashMap<String, CustomItem>();

}
