package me.mathiaseklund.ultimatesurvival.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.util.Util;
import net.md_5.bungee.api.ChatColor;

public class Crafting {

	static Inventory workbench;
	static HashMap<String, List<String>> recipes = new HashMap<String, List<String>>(); // id - "material amount"
	static HashMap<String, ItemStack> reciperesult = new HashMap<String, ItemStack>(); // recipe id - itemstack

	/**
	 * Open the Workbench GUI
	 * 
	 * @param player
	 *            Player who wants to open the Workbench
	 */
	public static void openWorkbench(Player player) {
		if (workbench != null) {
			Inventory inv = Bukkit.createInventory(player, workbench.getSize(), workbench.getTitle());
			inv.setContents(workbench.getContents());
			player.openInventory(inv);
		} else {
			Util.message(player,
					"&4ERROR:&7 Workbench Inventory not found. Please report this bug to a staff member &7&lASAP&7!");
		}
	}

	/**
	 * Create the Workbench GUI Template
	 */
	public static void setupWorkbench() {
		US main = US.getMain();
		workbench = Bukkit.createInventory(null, main.getConfig().getInt("gui.workbench.size"), Util.HideString("[wb]")
				+ ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("gui.workbench.title")));

		AddWBOutline();
		AddWBCraftOption();
	}

	/**
	 * Add the outline items to the workbench gui
	 */
	public static void AddWBOutline() {
		US main = US.getMain();
		ItemStack is = new ItemStack(
				Material.getMaterial(main.getConfig().getString("gui.workbench.outline.material")));
		ItemMeta im = is.getItemMeta();
		im.setLocalizedName(Util.HideString("[wb.outline]"));
		im.setDisplayName(Util.HideString("asd"));
		is.setItemMeta(im);
		String slots = main.getConfig().getString("gui.workbench.outline.slots");
		for (String s : slots.split(" ")) {
			int i = Integer.parseInt(s);
			workbench.setItem(i, is);
		}
	}

	/**
	 * Add the CraftOption items to the workbench gui
	 */
	public static void AddWBCraftOption() {
		US main = US.getMain();
		ItemStack is = new ItemStack(
				Material.getMaterial(main.getConfig().getString("gui.workbench.craftoption.material")));
		ItemMeta im = is.getItemMeta();
		im.setLocalizedName(Util.HideString("[wb.craftoption]"));
		im.setDisplayName(Util.HideString("asd"));
		is.setItemMeta(im);
		String slots = main.getConfig().getString("gui.workbench.craftoption.slots");
		for (String s : slots.split(" ")) {
			int i = Integer.parseInt(s);
			workbench.setItem(i, is);
		}
	}

	/**
	 * Add the CraftOption items to the specific inventory
	 */
	public static void AddWBCraftOption(Inventory inv) {
		US main = US.getMain();
		ItemStack is = new ItemStack(
				Material.getMaterial(main.getConfig().getString("gui.workbench.craftoption.material")));
		ItemMeta im = is.getItemMeta();
		im.setLocalizedName(Util.HideString("[wb.craftoption]"));
		im.setDisplayName(Util.HideString("asd"));
		is.setItemMeta(im);
		String slots = main.getConfig().getString("gui.workbench.craftoption.slots");
		for (String s : slots.split(" ")) {
			int i = Integer.parseInt(s);
			ItemStack item = inv.getItem(i);
			if (item != null) {
				ItemMeta itemmeta = item.getItemMeta();
				if (itemmeta.hasLocalizedName()) {
					String name = itemmeta.getLocalizedName();
					name = name.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
					if (!name.contains("wb.craftresult")) {
						inv.setItem(i, is);
					}
				}
			} else {
				inv.setItem(i, is);
			}

		}
	}

	/**
	 * Load all crafting recipes from config.
	 */
	public static void loadRecipes() {
		US main = US.getMain();
		Set<String> keys = main.getRecipes().getConfigurationSection("").getKeys(false);
		Util.debug("Found " + keys.size() + " Custom Recipes");
		recipes.clear();
		for (String s : keys) {
			recipes.put(s, main.getRecipes().getStringList(s + ".items"));
			ItemStack is = Items.getItem(main.getRecipes().getString(s + ".result"));
			if (is != null) {
				reciperesult.put(s, is);
			}
		}
	}

	public static void loadRecipe(String recipe) {
		US main = US.getMain();
		recipes.put(recipe, main.getRecipes().getStringList(recipe + ".items"));
		ItemStack is = Items.getItem(main.getRecipes().getString(recipe + ".result"));
		if (is != null) {
			reciperesult.put(recipe, is);
		}
	}

	/**
	 * Check if player has provided enough materials for any recipes
	 * 
	 * @param player
	 * @param wb
	 */
	public static void checkWBCraftOptions(Player player, Inventory wb) {
		US main = US.getMain();
		ArrayList<ItemStack> materials = new ArrayList<ItemStack>();
		for (ItemStack is : wb.getContents()) {
			if (is != null) {
				if (is.hasItemMeta()) {
					ItemMeta im = is.getItemMeta();
					if (im.hasLocalizedName()) {
						String dname = im.getLocalizedName();
						dname = dname.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
						if (dname.contains("[wb.outline]") || dname.contains("[wb.craftoption]")
								|| dname.contains("[wb.craftresult]")) {
							continue;
						}
					}
				}
				materials.add(is);
			}
		}
		ArrayList<String> foundRecipes = new ArrayList<String>();
		for (String recipe : recipes.keySet()) {
			if (!main.getRecipes().getBoolean(recipe + ".active")) {
				continue;
			}
			List<String> items = recipes.get(recipe);
			boolean hasMaterials = true;
			for (String item : items) {
				String mat = item.split(" ")[0];
				int amount = Integer.parseInt(item.split(" ")[1]);
				boolean found = false;
				int foundamount = 0;
				for (ItemStack is : materials) {
					if (is.hasItemMeta()) {
						ItemMeta im = is.getItemMeta();
						if (im.hasLocalizedName()) {
							String name = im.getLocalizedName();
							name = name.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
							if (name.contains("ultimatesurvival.item")) {
								String id = name.split(" ")[1];
								if (mat.equalsIgnoreCase(id)) {
									foundamount += is.getAmount();
									if (foundamount >= amount) {
										found = true;
										break;
									}
								}
							}
						}
					}
				}
				if (!found) {
					hasMaterials = false;
					break;
				}
			}
			if (hasMaterials) {
				foundRecipes.add(recipe);
				// Add craft option to wb gui
				if (reciperesult.containsKey(recipe)) {
					ItemStack result = new ItemStack(reciperesult.get(recipe));
					ItemMeta rm = result.getItemMeta();
					rm.setLocalizedName(Util.HideString("[wb.craftresult] " + recipe));
					result.setItemMeta(rm);
					for (int i = 0; i < wb.getSize(); i++) {
						ItemStack is = wb.getItem(i);
						if (is != null) {
							if (is.hasItemMeta()) {
								ItemMeta im = is.getItemMeta();
								if (im.hasLocalizedName()) {
									String dname = im.getLocalizedName();
									dname = dname.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
									if (dname.contains("[wb.craftresult]")) {
										if (dname.split(" ")[1].equalsIgnoreCase(recipe)) {
											break;
										}
										continue;
									} else if (dname.contains("[wb.craftoption]")) {
										wb.setItem(i, result);
										break;
									}
								}
							}
						}
					}
				} else {
					Util.debug(recipe + " does not have any results. Please set the crafting result");
				}
			}
		}
		for (int i = 0; i < wb.getSize(); i++) {
			ItemStack is = wb.getItem(i);
			if (is != null) {
				if (is.hasItemMeta()) {
					ItemMeta im = is.getItemMeta();
					if (im.hasLocalizedName()) {
						String dname = im.getLocalizedName();
						dname = dname.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
						if (dname.contains("wb.craftresult")) {
							String recipe = dname.split(" ")[1];
							if (!foundRecipes.contains(recipe)) {
								wb.setItem(i, null);
							}
						}
					}
				}
				materials.add(is);
			}
		}
		AddWBCraftOption(wb);
	}

	/**
	 * Craft the item and give it to the player.
	 * 
	 * @param player
	 * @param wb
	 * @param recipe
	 */
	public static void craftItem(Player player, Inventory wb, String recipe) {
		US main = US.getMain();
		if (!main.getRecipes().getBoolean(recipe + ".active")) {
			return;
		}
		for (String s : recipes.get(recipe)) {

			Util.debug("Current Recipe: " + recipe);
			String mat = s.split(" ")[0];
			int amount = Integer.parseInt(s.split(" ")[1]);
			for (int i = 0; i < wb.getSize(); i++) {
				ItemStack is = wb.getItem(i);
				if (is != null) {
					if (is.hasItemMeta()) {
						ItemMeta im = is.getItemMeta();
						if (im.hasLocalizedName()) {
							String dname = im.getLocalizedName();
							dname = dname.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
							if (dname.contains("[wb.craftresult] ")) {
								continue;
							} else if (dname.contains("[wb")) {
								continue;
							} else if (dname.contains("ultimatesurvival.item")) {
								String id = dname.split(" ")[1];
								if (id.equalsIgnoreCase(mat)) {
									if (is.getAmount() > amount) {
										is.setAmount(is.getAmount() - amount);
										amount = 0;
										break;
									} else if (is.getAmount() == amount) {
										wb.setItem(i, null);
										amount = 0;
										break;
									} else if (is.getAmount() < amount) {
										amount -= is.getAmount();
										wb.setItem(i, null);
									} else if (amount == 0) {
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		for (int i = 0; i < wb.getSize(); i++) {
			ItemStack is = wb.getItem(i);
			if (is != null) {
				if (is.hasItemMeta()) {
					ItemMeta im = is.getItemMeta();
					if (im.hasLocalizedName()) {
						String dname = im.getLocalizedName();
						dname = dname.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
						if (dname.contains("[wb.craftresult] ")) {
							String r = dname.split(" ")[1];
							if (r.equalsIgnoreCase(recipe)) {
								wb.setItem(i, null);

							}
							continue;
						}
					}
				}
			}
		}
		AddWBCraftOption(wb);
		player.getInventory().addItem(new ItemStack(reciperesult.get(recipe)));
	}

	/**
	 * Check if recipe exists.
	 * 
	 * @param recipe
	 * @return
	 */
	public static boolean recipeExists(String recipe) {
		if (recipes.containsKey(recipe)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Create custom recipe
	 * 
	 * @param recipe
	 */
	public static void createRecipe(String recipe) {
		US main = US.getMain();
		main.getRecipes().set(recipe + ".active", false);
		main.getRecipes().set(recipe + ".items", new ArrayList<String>());
		main.saveRecipes();
		loadRecipes();
	}

	/**
	 * Delete custom recipe
	 * 
	 * @param recipe
	 */
	public static void deleteRecipe(String recipe) {
		US main = US.getMain();
		if (recipeExists(recipe)) {
			main.getRecipes().set(recipe, null);
			main.saveRecipes();
			recipes.remove(recipe);
			reciperesult.remove(recipe);
			Util.debug("Custom Recipe deleted");
		}
	}

	/**
	 * Activate recipe
	 * 
	 * @param recipe
	 */
	public static void activateRecipe(String recipe) {
		US main = US.getMain();
		if (recipeExists(recipe)) {
			main.getRecipes().set(recipe + ".active", true);
			main.saveRecipes();
		}
	}

	/**
	 * Add material to itemstack
	 * 
	 * @param recipe
	 * @param itemId
	 * @param amount
	 */
	public static void addMaterial(String recipe, String itemId, int amount) {
		US main = US.getMain();
		List<String> materials = main.getRecipes().getStringList(recipe + ".items");
		materials.add(itemId + " " + amount);
		main.getRecipes().set(recipe + ".items", materials);
		main.saveRecipes();
		loadRecipe(recipe);

	}

	/**
	 * Remove material from recipe
	 * 
	 * @param recipe
	 * @param itemId
	 */
	public static void removeMaterial(String recipe, String itemId) {
		US main = US.getMain();
		List<String> materials = main.getRecipes().getStringList(recipe + ".items");
		for (int i = 0; i < materials.size(); i++) {
			String s = materials.get(i);
			if (s.split(" ")[0].equalsIgnoreCase(itemId)) {
				materials.remove(i);
				break;
			}
		}
		main.getRecipes().set(recipe + ".items", materials);
		main.saveRecipes();
		loadRecipe(recipe);
	}
}
