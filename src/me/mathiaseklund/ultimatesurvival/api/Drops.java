package me.mathiaseklund.ultimatesurvival.api;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import io.netty.util.internal.ThreadLocalRandom;
import me.mathiaseklund.ultimatesurvival.objects.CustomItem;
import me.mathiaseklund.ultimatesurvival.util.global;

public class Drops {

	/**
	 * Drop items for broken block
	 * 
	 * @param loc
	 * @param itemId
	 */
	public static void drop(Location loc, String itemId) {

		for (String s : Items.getDrops(itemId)) {
			ItemStack is = Items.getItem(s);
			loc.getWorld().dropItemNaturally(loc, is);
		}
	}

	/**
	 * Attempt to drop block drops
	 * 
	 * @param loc
	 * @param player
	 * @param itemId
	 */
	public static void tryDrop(Location loc, Player player, String itemId) {
		CustomItem ci = global.customitems.get(itemId);
		String tool = null;
		ItemStack is = player.getInventory().getItemInMainHand();
		if (is != null) {
			tool = Items.getItemId(is);
		}
		double dropchance = ci.getDropChance(tool);
		double random = ThreadLocalRandom.current().nextDouble(0.0, 100.0);
		if (dropchance >= random) {
			drop(loc, itemId);
		}
	}

}
