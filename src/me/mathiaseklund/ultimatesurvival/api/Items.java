package me.mathiaseklund.ultimatesurvival.api;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.ultimatesurvival.US;
import me.mathiaseklund.ultimatesurvival.objects.CustomItem;
import me.mathiaseklund.ultimatesurvival.util.Util;
import me.mathiaseklund.ultimatesurvival.util.global;

public class Items {

	/**
	 * Check if specified itemstack is a custom item
	 * 
	 * @param is
	 * @return true/false
	 */
	public static boolean isCustomItem(ItemStack is) {
		if (is.hasItemMeta()) {
			ItemMeta im = is.getItemMeta();
			if (im.hasLocalizedName()) {
				String name = im.getLocalizedName();
				name = name.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
				if (name.contains("[ultimatesurvival.item]")) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Get Custom item
	 * 
	 * @param id
	 * @return
	 */
	public static ItemStack getItem(String id) {
		CustomItem ci = global.customitems.get(id);
		if (ci != null) {
			return ci.getItem();
		} else {
			return null;
		}
	}

	/**
	 * Convert item to custom item if its a normal one.
	 * 
	 * @param is
	 *            item to convert
	 * @return ItemStack is
	 */
	public static ItemStack convertToCustomItem(ItemStack is) {
		if (is != null) {
			if (isCustomItem(is)) {
				return is;
			} else {
				CustomItem ci = new CustomItem(is);
				return ci.getItem();
			}
		} else {
			return null;
		}
	}

	/**
	 * Load all items
	 */
	public static void loadItems() {
		int c = 0;
		for (String s : US.getMain().getItems().getConfigurationSection("").getKeys(false)) {
			new CustomItem(s);
			c++;
		}
		Util.debug("Found " + c + " Custom Items");
	}

	/**
	 * Check if item exists.
	 * 
	 * @param itemId
	 * @return
	 */
	public static boolean itemExists(String itemId) {
		US main = US.getMain();
		if (main.getItems().get(itemId) != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Add drop to item
	 * 
	 * @param itemId
	 * @param dropId
	 */
	public static void addDrop(String itemId, String dropId) {
		US main = US.getMain();
		List<String> drops = main.getDrops().getStringList(itemId);
		if (!drops.contains(dropId)) {
			drops.add(dropId);
			main.getDrops().set(itemId, drops);
			main.saveDrops();
		}
	}

	/**
	 * Remove drop from item
	 * 
	 * @param itemId
	 * @param dropId
	 */
	public static void removeDrop(String itemId, String dropId) {
		US main = US.getMain();
		List<String> drops = main.getDrops().getStringList(itemId);
		if (drops.contains(dropId)) {
			drops.remove(dropId);
			main.getDrops().set(itemId, drops);
			main.saveDrops();
		}
	}

	/**
	 * Get item Id
	 * 
	 * @param is
	 * @return
	 */
	public static String getItemId(ItemStack is) {
		if (is.hasItemMeta()) {
			ItemMeta im = is.getItemMeta();
			if (im.hasLocalizedName()) {
				String name = im.getLocalizedName();
				name = name.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
				if (name.contains("ultimatesurvival.item")) {
					String itemId = name.split(" ")[1];
					return itemId;
				} else {
					return null;
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Get drops
	 * 
	 * @param id
	 * @return
	 */
	public static List<String> getDrops(String id) {
		US main = US.getMain();
		List<String> drops = main.getDrops().getStringList(id);

		return drops;
	}
}
